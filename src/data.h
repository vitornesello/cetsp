#ifndef _DATA_H_
#define _DATA_H_


#include <ilcplex/ilocplex.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <stdlib.h>
#include <string.h>
#include <list>
#include <vector>
#include <algorithm>
#include <iterator>
#include <math.h>
#include <limits>
#include <float.h>



using namespace std;

class Data{

	private:

	int sizeInst;
	double overlapRatio;
	char* options;
	double overlap_for_this_inst;
	double ub;

	vector< double > coordx;
	vector< double > coordy;
	vector< double > coordz;
	vector< double > radius;
	vector< double > demand;
	
	vector< vector < int > > intersecMatrix;
	vector< int > countIntersecs;
	void setIntersecMatrix();
	
	void setSizeInst( char * );

	void setDistanceMatrix();
	
	void setDepotFarthest();


	vector< vector< double > > distanceMatrix;
	vector< pair< int, double > > farthest;



	public:

	Data(int argc, char** argv);

	//	PRINT Functions
	void printInstance();	
	string fileName;


	//	SET Functions
	void setAllData( ifstream & );
	void setAllData2( ifstream & );
	void deleteZCoordinates();
	void setUb( string drctName, string instName, char * option, double overlap );


	//	GET Functions
	int getSizeInst();
	double getCoordx( int );
	double getCoordy( int );
	double getCoordz( int );
	double getRadius( int );
	double getDemand( int );
	int getBranchingRuleList( int );
	int getDepotFarthest( int );
	double getUbFromFile();
	double getOverlapRatio();


};

struct coordinates {
		double x;
		double y;
		double z;
};


#endif
