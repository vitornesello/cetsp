#include "data.h"

Data::Data(int argc, char **argv){

	// Carregando dados de entrada 
	if ( argc != 4 ) {
		cout << "It is not like that, check how it is done:\n";
		cout << "./exeCETSP [instace path] [options] [overlap ratio]" << endl;	
		exit( 1 );
	}



	char* instanceName = argv[1];
	options = argv[2];
	overlapRatio = atof(argv[3]);
	ifstream readFile( instanceName, ios::in );
		
	// Load data from file
	if ( !readFile ){
		cerr << " Deu pau pra abrir o arquivo .txt ... " << endl;
		exit( 1 );
	}
		
	// ========================================================================
	// identify reader by directory name
	string str = instanceName;
	string DirectoryName;
		
	string::size_type found = str.find_first_of("/");
	DirectoryName.append( instanceName, found );
  		
	string::size_type loc = str.find_last_of(".", str.size() );
	string::size_type loc2 = str.find_last_of("/", str.size() );

	if (loc != string::npos) {
		fileName.append(instanceName, loc2+1, loc-loc2-1 );
	}
	else {
		fileName.append(instanceName, loc2+1, str.size() );
	}
	// =========================================================================



	//		######### Mennel Instances ##########
	if ( DirectoryName.compare( "2D" ) == 0 || DirectoryName.compare( "3D" ) == 0 || DirectoryName.compare( "RND" ) == 0 ){
		setAllData( readFile );
		// if( DirectoryName.compare( "2D" ) == 0 ){
		// 	setUb( "2D", fileName, options, overlapRatio );
		// }
		// if( DirectoryName.compare( "3D" ) == 0 ){
		// 	setUb( "3D", fileName, options, overlapRatio );
		// }
		// if( DirectoryName.compare( "RND" ) == 0 ){
		// 	setUb( "RND", fileName, options, overlapRatio );
		// }
	}
	else{
		
		//			######### Behdani Instances ##########
		if ( DirectoryName.compare( "Behdani" ) == 0 ){
			setSizeInst( instanceName );
			setAllData2( readFile );
			// setUb( "Behdani", fileName, options, overlapRatio );
		}
		//			######### Behdani Instances ##########
		else{
			cout << "Something wrong with the directory names!" << endl;
			exit( 1 );
		}

					
	}
	//		######### Mennel Instances ##########
		

	//		finish identify reader by directory name

	if ( strcmp( options, "2D" ) == 0 ){
		deleteZCoordinates();
	}
				
	readFile.close();

	//		setIntersecMatrix();
	//		setBranchingRuleList();
	setDistanceMatrix();
	setDepotFarthest();
		
	//  compute overlap ratio for this instance
	overlap_for_this_inst = 0;
	double sum_radii = 0;
	for ( int i = 1; i < sizeInst; i++ ){
		sum_radii += radius[ i ];
	}
		
	double avg_radius = sum_radii/( sizeInst - 1 );
				
	vector< double > x_aux = coordx;
	vector< double > y_aux = coordy;
	vector< double > z_aux = coordz;
	//		
	sort( x_aux.begin(), x_aux.end() );
	sort( y_aux.begin(), y_aux.end() );
	sort( z_aux.begin(), z_aux.end() );
		
	vector< double > largest_coord;
	largest_coord.push_back(x_aux[ sizeInst - 1 ]);
	largest_coord.push_back(y_aux[ sizeInst - 1 ]);
	largest_coord.push_back(z_aux[ sizeInst - 1 ]);
				
	sort( largest_coord.begin(), largest_coord.end() );
		
	overlap_for_this_inst = avg_radius/largest_coord[ 2 ];
	//finish set overlap for this instance		
}
	
void Data::setUb( string drctName, string instName, char * option, double overlap )
{	
	string tempString;
	cout << "Nome: " << instName << endl;
	cout << "diretorio: " << drctName << endl;
	cout << "opção: " << option << endl;
		
	cout << "1" << endl;
	if( drctName.compare( "2D" ) == 0 ){
		if( strcmp( option, "3D" ) == 0 ){
			cout << "Inconsistent Option For This Instances!" << endl;
			exit( 1 );
		}
		if( strcmp( option, "2D" ) == 0 ){
			if( overlap == 1.0 ){
				ifstream readFile( "Upper_Bounds/2D10.txt", ios::in );
				while ( tempString.compare( instName ) != 0 ){
					readFile >> tempString;
					//						cout << tempString << endl;getchar();
				}
				readFile >> tempString;
				ub = atof( tempString.c_str() );
			}
			else{
				cout << "Wrong Overlap Factor!" << endl;
				exit( 1 );
			}
		}
	}
	cout << "2" << endl;
	if( drctName.compare( "3D" ) == 0 ){
		if( strcmp( option, "2D" ) == 0 ){
			//		cout << "teste: " << endl;
			if( overlap >= 0.1 ){
				ifstream readFile( "Upper_Bounds/2D01.txt", ios::in );
				while ( tempString.compare( instName ) != 0 ){
					readFile >> tempString;
					//						cout << tempString << endl;getchar();
				}
				readFile >> tempString;
				ub = atof( tempString.c_str() );
			}
			if( overlap >= 0.5 ){
				ifstream readFile( "Upper_Bounds/2D05.txt", ios::in );
				while ( tempString.compare( instName ) != 0 ){
					readFile >> tempString;
					//						cout << tempString << endl;getchar();
				}
				readFile >> tempString;
				ub = atof( tempString.c_str() );
			}
			if( overlap >= 1.5 ){
				ifstream readFile( "Upper_Bounds/2D15.txt", ios::in );
				while ( tempString.compare( instName ) != 0 ){
					readFile >> tempString;
					//						cout << tempString << endl;getchar();
				}
				readFile >> tempString;
				ub = atof( tempString.c_str() );
			}
			if( overlap < 0.1 ){
				cout << "Wrong Overlap Factor! Please choose something >= 0.1" << endl;
				exit( 1 );
			}
		}
		if( strcmp( option, "3D" ) == 0 ){
			if( overlap == 0.1 ){
				ifstream readFile( "Upper_Bounds/3D01.txt", ios::in );
				while ( tempString.compare( instName ) != 0 ){
					readFile >> tempString;
					//						cout << tempString << endl;getchar();
				}
				readFile >> tempString;
				ub = atof( tempString.c_str() );
			}
			if( overlap == 0.5 ){
				ifstream readFile( "Upper_Bounds/3D05.txt", ios::in );
				while ( tempString.compare( instName ) != 0 ){
					readFile >> tempString;
					//						cout << tempString << endl;getchar();
				}
				readFile >> tempString;
				ub = atof( tempString.c_str() );
			}
			if( overlap == 1.5 ){
				ifstream readFile( "Upper_Bounds/3D15.txt", ios::in );
				while ( tempString.compare( instName ) != 0 ){
					readFile >> tempString;
					//						cout << tempString << endl;getchar();
				}
				readFile >> tempString;
				ub = atof( tempString.c_str() );
			}
			if( overlap != 0.1 && overlap != 0.5 && overlap != 1.5 ){
				cout << "Wrong Overlap Factor!" << endl;
				exit( 1 );
			}
		}
	}
	cout << "3" << endl;

	if( drctName.compare( "RND" ) == 0 ){
		if( strcmp( option, "2D" ) == 0 ){
			if( overlap == 1.0 ){
				ifstream readFile( "Upper_Bounds/2D10.txt", ios::in );
				while ( tempString.compare( instName ) != 0 ){
					readFile >> tempString;
					//						cout << tempString << endl;getchar();
				}
				readFile >> tempString;
				ub = atof( tempString.c_str() );
			}
			else{
				cout << "Wrong Overlap Factor!" << endl;
				exit( 1 );
			}
		}
		if( strcmp( option, "3D" ) == 0 ){
			if( overlap == 1.0 ){
				ifstream readFile( "Upper_Bounds/3D10.txt", ios::in );
				while ( tempString.compare( instName ) != 0 ){
					readFile >> tempString;
					//						cout << tempString << endl;getchar();
				}
				readFile >> tempString;
				ub = atof( tempString.c_str() );
			}
			else{
				cout << "Wrong Overlap Factor!" << endl;
				exit( 1 );
			}
		}
	}
	cout << "4" << endl;

	if( drctName.compare( "Behdani" ) == 0 ){
		if( 0 >= overlap || overlap >= 1 ){
			cout << "Wrong Overlap Factor!" << endl;
			exit( 1 );
		}
		if( strcmp( option, "2D" ) == 0 ){
			ub = 999;
		}
		if( strcmp( option, "3D" ) == 0 ){
			cout << "Inconsistent Option For This Instances!" << endl;
			exit( 1 );
		}			
	}
}
	
double Data::getUbFromFile()
{
	return ub;
}

//	====== SET Functions ======
void Data::setAllData( ifstream &readFile )
{
	sizeInst = 0;	
	string trashString;		
	//		long int location = 0;
	    	
	while ( trashString.compare("//Depot") != 0 && trashString.compare("//Depot:") != 0 ) {
		readFile >> trashString;
	}
	    	
	//	    	########### read depot coordinates ############

	//		get out the "is"
	if( trashString.compare("//Depot:") != 0 ){
		readFile >> trashString;
	}
		
	//		shows the position of readFile pointer
	//		location = readFile.tellg();
	    	
	double temp[ 3 ];	
	readFile >> temp[ 0 ] >> trashString >> temp[ 1 ] >> trashString >> temp[ 2 ];
	coordx.push_back(temp[ 0 ]);
	coordy.push_back(temp[ 1 ]);
	coordz.push_back(temp[ 2 ]);
	radius.push_back( 0 );
	demand.push_back( 0 );	
	sizeInst++;
		
	//		set readFile ptr to 0
	readFile.seekg( 0 );
		
	//		##########  read customers coordinates	  #################
	while ( trashString.compare("description") != 0 ) {
		readFile >> trashString;
	}
		
	readFile >> trashString;
	
	while ( trashString.compare("//Depot") != 0 && trashString.compare("//Depot:") != 0 ) {
		
		double temp = atof(trashString.c_str());
		coordx.push_back(temp);
		readFile >> temp;
		coordy.push_back(temp);
		readFile >> temp;
		coordz.push_back(temp);
		readFile >> temp; 
		radius.push_back(temp);
		readFile >> temp;
		demand.push_back(temp);
		sizeInst++;
		readFile >> trashString;
	}
		
	//		multiply ratio by the overlapFactor
	for ( int i = 0; i < sizeInst; i++ ){
		radius[ i ] = radius[ i ]*overlapRatio;
	}	
}

	
void Data::setAllData2( ifstream &readFile )
{
	double x = 0;
	double y = 0;
	double z = 0;
		
	for ( int i = 0; i < sizeInst; i++ ){
		readFile >> x >> y;
		coordx.push_back( x );
		coordy.push_back( y );
		coordz.push_back( 0 );
		radius.push_back( 1 );
		demand.push_back( 0 );
	}
		
	radius[ 0 ] = 0;
		
	//		multiply ratio by the overlapFactor
	for ( int i = 0; i < sizeInst; i++ ){
		radius[ i ] = radius[ i ]*overlapRatio;
	}
	//		cout << "size: " << sizeInst << endl;
	//		for ( int i = 0; i < sizeInst; i++ ){
	//			cout << coordx[ i ] << " " << coordy[ i ] << " " << coordz[ i ] << " " << radius[ i ] << " " << demand[ i ] << endl;
	//		}
	//		cout << endl;
	//		getchar();
}

void Data::setSizeInst( char * instancePath )
{
	char * pch;
	int count = 0;
	pch = strtok ( instancePath, "/-");
	while ( pch != NULL ){
		count++;
		pch = strtok ( NULL, "/-");
		if( count == 3 ) sizeInst = atoi( pch ); 
	}
	//  	count on the DEPOT
	sizeInst++;
}

double euclidianDistance( coordinates * p1, coordinates * p2 )
{
	return sqrt( pow( p2->x - p1->x, 2) + pow( p2->y - p1->y, 2) + pow( p2->z - p1->z, 2) );
}
void Data::deleteZCoordinates()
{
	for ( int i = 0; i < sizeInst; i++ ){
		coordz[ i ] = 0;
	}
	cout << "OPTION 2D ACTIVATED, Z COORDINATES BECAME 0" << endl;
}

void Data::setDistanceMatrix()
{
	distanceMatrix.resize( sizeInst );
		
	for ( int i = 0; i < sizeInst; i++ ){
		distanceMatrix[ i ].resize( sizeInst );
	}
		
	coordinates * p1 = new coordinates;
	coordinates * p2 = new coordinates;
		
	for ( int i = 0; i < sizeInst; i++ ){
		for ( int j = 0; j < sizeInst; j++ ){
			p1->x = coordx[ i ];
			p1->y = coordy[ i ];
			p1->z = coordz[ i ];
				
			p2->x = coordx[ j ];
			p2->y = coordy[ j ];
			p2->z = coordz[ j ];				
				
			distanceMatrix[ i ][ j ] = euclidianDistance( p1, p2 );
		}
	}
		
	delete p1;
	delete p2;
				
	//		for ( int i = 0; i < sizeInst; i++ ){
	//			for ( int j = 0; j < sizeInst; j++ ){
	//				cout << distanceMatrix[ i ][ j ] << " ";
	//			}
	//			cout << endl;
	//		}
}
	
bool simpleSortVector ( pair< int, double > i, pair< int, double > j) { return ( i.second > j.second ); }
	
void Data::setDepotFarthest()
{
	pair< int, double > temp;
		
	for ( int i = 0; i < sizeInst; i++ ){
		temp = make_pair( i, distanceMatrix[ 0 ][ i ] );
		farthest.push_back( temp );
	}
		
	sort ( farthest.begin(), farthest.end(), simpleSortVector );
		
	//		for ( int i = 0; i < sizeInst; i++ ){
	//			cout << farthest[ i ].first << " ";
	//		}
	//		cout << endl;
}
	
int Data::getDepotFarthest( int i )
{
	return farthest[ i ].first;
}
	
int Data::getSizeInst()
{
	return sizeInst;	
}
	
double Data::getCoordx( int i )
{
	return coordx[ i ];	
}
	
double Data::getCoordy( int i )
{
	return coordy[ i ];	
}
	
double Data::getCoordz( int i )
{
	return coordz[ i ];	
}
	
double Data::getRadius( int i )
{
	return radius[ i ];	
}
	
double Data::getDemand( int i )
{
	return demand[ i ];	
}




void Data::printInstance()
{
	// for ( int i = 0; i < sizeInst; i++ ){
	// 	cout << coordx[ i ] << " " << coordy[ i ] << " " << radius[ i ] << " " << demand[ i ]<< endl;
	// }

	cout << "N: " << sizeInst << endl;
	for ( int i = 0; i < sizeInst; i++ ){
		cout << coordx[ i ] << " " << coordy[ i ] << " " << coordz[ i ] << " " << radius[ i ] << " " << demand[ i ]<< endl;
	}


	// for ( int i = 0; i < sizeInst; i++ ){
	// 	cout << coordx[ i ] << " " << coordy[ i ] << " " << coordz[ i ] << " " << demand[ i ]<< endl;

	// }

}

