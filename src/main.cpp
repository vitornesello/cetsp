

#include "data.h"


int main(int argc, char** argv){

	Data data(argc, argv);
	data.printInstance();


	IloEnv env;
	// Create model
	try{
		IloModel model(env);
		IloCplex cplex(model);
		char var[100];


		// CREATE VARIABLES
		// Create b_it
		IloArray <IloBoolVarArray> b(env, data.getSizeInst() + 1);
		for (int j = 0; j < data.getSizeInst() + 1; j++){
			IloBoolVarArray array(env, data.getSizeInst() + 1);
			b[j] = array;
		}

		IloNumVarArray s_x(env, data.getSizeInst() + 1, -IloInfinity, IloInfinity);
		IloNumVarArray s_y(env, data.getSizeInst() + 1, -IloInfinity, IloInfinity);
		IloNumVarArray s_z(env, data.getSizeInst() + 1, -IloInfinity, IloInfinity);

		IloNumVarArray d(env, data.getSizeInst() + 1, 0, IloInfinity);


		// Adding variables to the model
		b[0][0].setName("b(0,0)");
		model.add(b[0][0]);
		sprintf(var, "b(%d,%d)", data.getSizeInst(), data.getSizeInst());
		b[data.getSizeInst()][data.getSizeInst()].setName(var);
		model.add(b[data.getSizeInst()][data.getSizeInst()]);
		for (int j = 1; j < data.getSizeInst(); j++){
			for (int t = 1; t < data.getSizeInst(); t++){
				//if (j != t){
				sprintf(var, "b(%d,%d)", j, t);
				b[j][t].setName(var);
				model.add(b[j][t]);
				//}
			}
		}
      
		IloArray <IloNumVarArray> rx(env, data.getSizeInst());
		for (int j = 0; j < data.getSizeInst(); j++){
			IloNumVarArray array(env, data.getSizeInst(),-IloInfinity,IloInfinity);
			rx[j] = array;
		}

		IloArray <IloNumVarArray> ry(env, data.getSizeInst());
		for (int j = 0; j < data.getSizeInst(); j++){
			IloNumVarArray array(env, data.getSizeInst(),-IloInfinity,IloInfinity);
			ry[j] = array;
		}

		IloArray <IloNumVarArray> rz(env, data.getSizeInst());
		for (int j = 0; j < data.getSizeInst(); j++){
			IloNumVarArray array(env, data.getSizeInst(),-IloInfinity,IloInfinity);
			rz[j] = array;
		}


		for (int i = 1; i < data.getSizeInst(); i++){
			for (int t = 1; t < data.getSizeInst(); t++){
				sprintf (var, "r_x(%d,%d)",i,t);
				rx[i][t].setName(var);
				model.add(rx[i][t]);
			}
		}
		for (int i = 1; i < data.getSizeInst(); i++){
			for (int t = 1; t < data.getSizeInst(); t++){
				sprintf (var, "r_y(%d,%d)",i,t);
				ry[i][t].setName(var);
				model.add(ry[i][t]);
			}
		}
		for (int i = 1; i < data.getSizeInst(); i++){
			for (int t = 1; t < data.getSizeInst(); t++){
				sprintf (var, "r_z(%d,%d)",i,t);
				rz[i][t].setName(var);
				model.add(rz[i][t]);
			}
		}

		for (int t = 0; t < data.getSizeInst() + 1; t++){
			sprintf(var, "s_x(%d)", t);
			s_x[t].setName(var);
			model.add(s_x[t]);

			sprintf(var, "s_y(%d)", t);
			s_y[t].setName(var);
			model.add(s_y[t]);

			sprintf(var, "s_z(%d)", t);
			s_z[t].setName(var);
			model.add(s_z[t]);
		}

		for (int t = 0; t < data.getSizeInst(); t++){
			sprintf(var, "d(%d)", t);
			d[t].setName(var);
			model.add(d[t]);
		}



		IloExpr objFunction(env);

		for (int t = 0; t < data.getSizeInst(); t++){
			objFunction += d[t];
		}

		model.add(IloMinimize(env, objFunction));


		// Cover constraints
		for (int i = 1; i < data.getSizeInst(); i++){
			IloExpr exp (env);
			sprintf (var, "Cov1_(%d)", i);
			for (int t = 1; t < data.getSizeInst(); t++){
				exp += b[i][t];
			}
			IloRange cons = (exp == 1);
			cons.setName(var);
			model.add(cons);
		}
		// Cover constraints
		for (int t = 1; t < data.getSizeInst(); t++){
			IloExpr exp (env);
			sprintf (var, "Cov2_(%d)", t);
			for (int i = 1; i < data.getSizeInst(); i++){
				exp += b[i][t];
			}
			IloRange cons = (exp == 1);
			cons.setName(var);
			// model.add(cons);
		}


		IloNumVarArray dx(env, data.getSizeInst(), -IloInfinity, IloInfinity);
		IloNumVarArray dy(env, data.getSizeInst(), -IloInfinity, IloInfinity);
		IloNumVarArray dz(env, data.getSizeInst(), -IloInfinity, IloInfinity);


		for (int t = 0; t < data.getSizeInst(); t++){
			sprintf (var, "dX_(%d)", t);
			dx[t].setName(var);
			model.add(dx[t]);
		}
		for (int t = 0; t < data.getSizeInst(); t++){
			sprintf (var, "dY_(%d)", t);
			dy[t].setName(var);
			model.add(dy[t]);
		}
		for (int t = 0; t < data.getSizeInst(); t++){
			sprintf (var, "dZ_(%d)", t);
			dz[t].setName(var);
			model.add(dz[t]);
		}

		for (int t = 0; t < data.getSizeInst(); t++){
			sprintf (var, "deltaX_(%d)", t);
			IloRange cons = ( dx[t] - s_x[t+1] + s_x[t] == 0 );
			cons.setName(var);
			model.add(cons);
		}
		for (int t = 0; t < data.getSizeInst(); t++){
			sprintf (var, "deltaY_(%d)", t);
			IloRange cons = ( dy[t] - s_y[t+1] + s_y[t] == 0 );
			cons.setName(var);
			model.add(cons);
		}
		for (int t = 0; t < data.getSizeInst(); t++){
			sprintf (var, "deltaZ_(%d)", t);
			IloRange cons = ( dz[t] - s_z[t+1] + s_z[t] == 0 );
			cons.setName(var);
			model.add(cons);
		}

		// Compute distance (SOCP)
		for (int t = 0; t < data.getSizeInst(); t++){
			sprintf (var, "C2_(%d)", t);
			IloRange cons = (-d[t]*d[t] + dx[t]*dx[t] + dy[t]*dy[t] + dz[t]*dz[t] <= 0);
			cons.setName(var);
			model.add(cons);
		}


      

		for (int i = 1; i < data.getSizeInst(); i++){
			for (int t = 1; t < data.getSizeInst(); t++){
				sprintf (var, "radX_(%d,%d)", i, t);
				IloRange cons = ( rx[i][t] - data.getCoordx(i) + s_x[t] == 0 );
				cons.setName(var);
				model.add(cons);
			}
		}
		for (int i = 1; i < data.getSizeInst(); i++){
			for (int t = 1; t < data.getSizeInst(); t++){
				sprintf (var, "rady_(%d,%d)", i, t);
				IloRange cons = ( ry[i][t] - data.getCoordy(i) + s_y[t] == 0 );
				cons.setName(var);
				model.add(cons);
			}
		}
		for (int i = 1; i < data.getSizeInst(); i++){
			for (int t = 1; t < data.getSizeInst(); t++){
				sprintf (var, "radZ_(%d,%d)", i, t);
				IloRange cons = ( rz[i][t] - data.getCoordz(i) + s_z[t] == 0 );
				cons.setName(var);
				model.add(cons);
			}
		}

		// Radius constraint (SOCP)
		for (int i = 1; i < data.getSizeInst(); i++){
			for (int t = 1; t < data.getSizeInst(); t++){
				double radiusi = data.getRadius(i);
				sprintf (var, "C3_(%d,%d)", i, t);
				IloRange cons = (  rx[i][t]*rx[i][t] 
								   + ry[i][t]*ry[i][t] 
								   + rz[i][t]*rz[i][t] 
 								   + 999*(b[i][t] - 1) <= radiusi*radiusi);
				cons.setName(var);
				model.add(cons);
				//}
			}
		}

		model.add(b[0][0] == 1);
		model.add(b[data.getSizeInst()][data.getSizeInst()] == 1);
		model.add(s_x[0] == data.getCoordx(0));
		model.add(s_y[0] == data.getCoordy(0));
		model.add(s_z[0] == data.getCoordz(0));
		model.add(s_x[data.getSizeInst()] == data.getCoordx(0));
		model.add(s_y[data.getSizeInst()] == data.getCoordy(0));
		model.add(s_z[data.getSizeInst()] == data.getCoordz(0));




		cplex.exportModel("CETSP.lp");

		cplex.setParam(IloCplex::Threads, 1);
		cplex.setParam(IloCplex::TreLim, 10000);
		cplex.setParam(IloCplex::TiLim, 3600);
		// cplex.setParam(IloCplex::EpGap, 0.0000000001);
		cplex.setParam(IloCplex::EpInt, 0.0000000001);
		// cplex.setParam(IloCplex::EpOpt, 0.2);
		// cplex.setParam(IloCplex::NodeLim, 1);

		// double time = cpuTime();
		bool feasible = cplex.solve();
		double time = cplex.getTime();
		// time = cpuTime() - time;


		// cout << "Solution:" << endl;
		// for (int i = 1; i < data.getSizeInst(); i++){
		// 	for (int j = 1; j < data.getSizeInst(); j++){
		// 		cout << "b(" << i << "," << j << "): " << cplex.getValue(b[i][j]) << endl;
		// 	}
		// }



		cout << "\nStatus: "<< cplex.getCplexStatus() << endl;
		cout << "Objective Function: " << cplex.getObjValue() << endl;


	}catch (IloException& e){
		cerr << "Concert exception caught: " << e << endl;
	}catch (...){
		cerr << "Unknown exception caught" << endl;
	}



	return 0;
}

